﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class KeyboardPhysicsMovement : MonoBehaviour {
    public float maxSpeed = 3f;
    [Tooltip("Speed gain per second")]
    public float acceleration = 3f;

    // Public only for demonstration purposes.
    public bool up;
    public bool down;
    public bool right;
    public bool left;

    private Rigidbody2D body;

    protected void Awake() {
        body = GetComponent<Rigidbody2D>();
    }

    protected void Update() {
        // Read input.
        up = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
        down = Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);
        right = Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
        left = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);

        // Calculate speed gained in this frame.
        var speed = acceleration * Time.deltaTime;

        // Update velocity.
        var velocity = body.velocity;
        if (up) {
            velocity.y += speed;
        }
        if (down) {
            velocity.y -= speed;
        }
        if (right) {
            velocity.x += speed;
        }
        if (left) {
            velocity.x -= speed;
        }
        // Limit velocity by speed.
        if (velocity.sqrMagnitude > maxSpeed * maxSpeed) {
            velocity = velocity.normalized * maxSpeed;
        }

        body.velocity = velocity;
    }
}
