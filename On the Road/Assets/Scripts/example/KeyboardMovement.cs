﻿using UnityEngine;

public class KeyboardMovement : MonoBehaviour {
    public float speed = 3f;

    // Public only for demonstration purposes.
    public bool up;
    public bool down;
    public bool right;
    public bool left;

    // Cache the transform component instead of using the provided property
    // 'transform' to avoid the implicit call to GetComponent every time.
    private new Transform transform;

    protected void Awake() {
        transform = GetComponent<Transform>();
    }

    protected void Update() {
        // Read input.
        up = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
        down = Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);
        right = Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
        left = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);

        // Calculate distance to travel in this frame.
        var distance = speed * Time.deltaTime;

        // Update position.
        var position = transform.position;
        if (up) {
            position.y += distance;
        }
        if (down) {
            position.y -= distance;
        }
        if (right) {
            position.x += distance;
        }
        if (left) {
            position.x -= distance;
        }
        transform.position = position;
    }
}
