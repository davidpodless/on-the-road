﻿using UnityEngine;
using UnityEditor;
using Infra.Utils;

[CustomPropertyDrawer(typeof(EnumBasedStringAttribute))]
public class EnumBasedStringPropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        label = EditorGUI.BeginProperty(position, label, property);

        if (property.propertyType != SerializedPropertyType.String) {
            EditorGUI.LabelField(position, "ERROR:", "May only apply to type string");
            return;
        }

        position = EditorGUI.PrefixLabel(position, label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        if (GUI.Button(position, property.stringValue, EditorStyles.popup)) {
            var enumAttribute = attribute as EnumBasedStringAttribute;
            var menu = new GenericMenu();
            foreach (var value in enumAttribute.stringValues) {
                menu.AddItem(new GUIContent(value), value == property.stringValue, enumValue => {
                    property.stringValue = enumValue.ToString();
                    property.serializedObject.ApplyModifiedProperties();
                }, value);
            }
            menu.ShowAsContext();
        }

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
}