using UnityEngine;
using System;

namespace Infra.Utils {
public class EnumBasedStringAttribute : PropertyAttribute {
    public string[] stringValues;

    public EnumBasedStringAttribute(Type enumType) {
        if (!enumType.IsEnum) {
            throw new ArgumentException("enumType must be an enumerated type");
        }

        var values = Enum.GetValues(enumType);
        stringValues = new string[values.Length];

        for (int i = 0; i < values.Length; i++) {
            stringValues[i] = values.GetValue(i).ToString();
        }
    }
}
}
