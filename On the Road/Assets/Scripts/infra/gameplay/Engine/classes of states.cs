﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace OnTheRoad {
    public class BoolDictionary : Dictionary<string, bool> { }
    public class FloatDictionary : Dictionary<string, float> { }
    public class ItemDictionary : Dictionary<string, Item> { }
    class BoolState : state<bool> {  }

    

    class NumState: state<float> {
        
        public void addOne(string key) {
            try {
                SetState(key, FindState(key) + 1);
            } catch (Exception) {
                Debug.Log("Could not find the Key " + key);
            }
            
        }
    }

    class ItemState: state<Item> {
        public bool HasItems() {
            return stateDic.Count > 0;
        }

        public List<string> Items() {
            return new List<string>(stateDic.Keys);
        }
    }

    public class AllStates : ScriptableObject {
        
        private BoolState boolState;
        private NumState numState;
        private ItemState itemState;

        void Awake() {
            boolState = new BoolState();
            numState = new NumState();
            itemState = new ItemState();

    }
        public void AddOne(string Key) {
            try {
                numState.FindState(Key);
                numState.addOne(Key);
            } catch (Exception) {
                if (FindState(Key) != null)
                    Debug.Log("Can't add one to the key " + Key);
            }
        }

        public void SetState(string Key, bool Value) {
            boolState.SetState(Key, Value);
        }
        
        public void SetState(string Key, float Value) {
            numState.SetState(Key, Value);
        }
        
        public void SetState(string Key, Item Value) {
            Key = Value.NameOfItem;
            itemState.SetState(Key, Value);
        }

        public object FindState(string key) {
            try {
                return boolState.FindState(key);
            }
            catch (Exception) {
                try {
                    return numState.FindState(key);
                }
                catch (Exception) {
                    try {
                        return itemState.FindState(key);
                    }
                    catch (Exception) {
                        Debug.Log("Could not find the Key " + key);
                        throw new KeyNotFoundException(key);
                    }
                }
            }
        }

        public bool hasItems() {
            return itemState.HasItems();
        }

        public List<string> ItemsStateKey() {
            return itemState.Items();
        }

        public bool RemoveState(string Key) {
            return (boolState.RemoveKey(Key) || numState.RemoveKey(Key) || itemState.RemoveKey(Key)) ;
        }
    }
}
