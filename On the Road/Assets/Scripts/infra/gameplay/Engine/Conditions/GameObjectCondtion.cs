﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    class GameObjectCondtion : Condition{
        [SerializeField]
        GameObject gameobject;
        [SerializeField]
        bool value;
        public override bool isMet(InteractableObject IO) {
            return gameobject.activeSelf == value;

        }

    }
}
