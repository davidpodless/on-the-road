﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    class PlayerConditions : Condition{
        private enum boolOrFloat {
            Bool,
            Float,
        }
        [SerializeField]
        boolOrFloat choose;
        [SerializeField]
        bool boolValue;
        [SerializeField]
        float floatValue;
        
        private GameObject player;
        private InteractableObject playerIO;

        private void Start() {
            player = GameObject.FindWithTag("Player");
            playerIO = player.GetComponent<InteractableObject>();
        }

        public override bool isMet(InteractableObject IO) {
            switch (choose) {
                case boolOrFloat.Bool:
                    bool boolGoal = (bool)playerIO.FindState(Key);
                    switch (Op) {
                        case operations.Equals:
                            return boolValue == boolGoal;
                        case operations.NotEquals:
                            return boolValue != boolGoal;
                        default:
                            throw new UnsupportedOperationException();
                    }
                case boolOrFloat.Float:
                    float floatGoal = (float)playerIO.FindState(Key);
                    switch (Op) {
                        case operations.Equals:
                            return floatValue == floatGoal;
                        case operations.NotEquals:
                            return floatValue != floatGoal;
                        case operations.BiggerThan:
                            return floatGoal > floatValue;
                        case operations.LessThan:
                            return floatGoal < floatValue;
                        default:
                            throw new UnsupportedOperationException();
                    }
                default:
                    throw new UnsupportedOperationException();
            }
            
        }

    }
}
