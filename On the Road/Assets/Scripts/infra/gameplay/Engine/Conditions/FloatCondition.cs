﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    class FloatCondition : Condition{

        [SerializeField]
        float value;

        public override bool isMet(InteractableObject IO) {
            float goal = (float)IO.FindState(Key);
            switch (Op) {
                case operations.Equals:
                    return value == goal;
                case operations.NotEquals:
                    return value != goal;
                case operations.BiggerThan:
                    return goal > value;
                case operations.LessThan:
                    return goal < value;
                default:
                    throw new UnsupportedOperationException();
            }
        }

    }
}
