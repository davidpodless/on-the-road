﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    class BoolCondition : Condition{

        [SerializeField]
        bool value;

        public override bool isMet(InteractableObject IO) {
            bool goal = (bool)IO.FindState(Key);
            switch (Op) {
                case operations.Equals:
                    return value == goal;
                case operations.NotEquals:
                    return value != goal;
                default:
                    throw new UnsupportedOperationException();
            }
        }

    }
}
