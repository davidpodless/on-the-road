﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Infra.Utils;

namespace OnTheRoad {
    class ItemConditionInventory : Condition{
        [EnumBasedString(typeof(Item.Items))]
        [SerializeField]
        string item;
        private GameObject player;
        protected Inventory inventory;

        public override bool isMet(InteractableObject IO) {
            switch (Op) {
                case operations.Equals:
                    if (inventory.ItemInHand == null)
                        return item == Item.Items.Nothing.ToString();
                    return inventory.ItemInHand.NameOfItem.Equals(item);
                case operations.NotEquals:
                    if (inventory.ItemInHand == null)
                        return item != Item.Items.Nothing.ToString();
                    return !inventory.ItemInHand.NameOfItem.Equals(item);
                default:
                    throw new UnsupportedOperationException();
            }
        }

        protected virtual void Start() {
            player = GameObject.FindWithTag("Player");
            inventory = player.GetComponentInChildren<Inventory>();
        }

    }
}
