﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infra.Utils;

namespace OnTheRoad {
    abstract public class Condition : MonoBehaviour {

        public enum operations {
            NotEquals,
            Equals,
            LessThan,
            BiggerThan
        }

        //[EnumBasedString(typeof(operations))]
        [SerializeField]
        operations op;
        [EnumBasedString(typeof(StateKey))]
        [SerializeField] string key;

        public operations Op { get { return op; } }
        public string Key { get { return key; } }

        /**
         * check if the conditiion is met 
         */
        public abstract bool isMet(InteractableObject IO);


    }
}