﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infra.Utils;

namespace OnTheRoad {
    [CreateAssetMenu(menuName = "OnTheRoad/Item")]
    public class Item : ScriptableObject {
        public enum Items {
            Sword,
            Rope,
            Fuel,
            Nothing,
        }
        [SerializeField]
        Sprite inventoryIcon;

        [EnumBasedString(typeof(Items))]
        [SerializeField]
        string nameOfItem;

        public AllStates states;

        private void Awake() {
            states = CreateInstance<AllStates>();
        }

        public string NameOfItem { get { return nameOfItem; } }

        public Sprite InventoryIcon { get { return inventoryIcon; } }
       

    }

}
