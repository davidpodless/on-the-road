﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Infra.Utils;

namespace OnTheRoad {
    public enum StateKey {
        HasExamine,
        WentToSleep,
        PlayerHere,
        NumOfLoots,
        triedToAttack,
        wantedToHelp,
        NathanEnteredToHome,
        triesToGoBack,
        NathanDiedBy,
        wasHere,
        Chapter,
        EdwerdTalkToNathan,
    }

    [Serializable]
    public class stringAndT<T> {

        public string Key;

        public T Value;


    }

    [Serializable]
    public class stringAndbool {
        [EnumBasedString(typeof(StateKey))]
        public string Key;

        public bool Value;
    }

    [Serializable]
    public class stringAndFloat {
        [EnumBasedString(typeof (StateKey))]
        public string Key;

        public float Value;
    }

    [Serializable]
    public class stringAndItem {

        public string Key;

        public Item Value;
    }
}
