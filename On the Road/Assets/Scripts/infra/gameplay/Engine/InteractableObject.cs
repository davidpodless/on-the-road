﻿//using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace OnTheRoad {
    abstract public class InteractableObject : MonoBehaviour  {
        private const float BelowTheObject = -0.7f;

        private ClickMovement move;
        private GameObject player;
        protected Thoughts examineThoghut;
        [SerializeField]
        List<stringAndbool> boolList;
        [SerializeField]
        List<stringAndFloat> floatList;
        /*[SerializeField]
        List<stringAndItem> itemList;*/ // I don't think that I need it.

        public List<ActionAndConditions> possibleActions;

        [SerializeField] string[] examines;

        private string lastExamine;
        public string Examine {
            get { return examines[0]; }
            set { lastExamine = value; }
        }

        private AllStates states;

        public object FindState(string Key) {
            return states.FindState(Key);

        }

        public void SetState(string Key, bool Value) {
            states.SetState(Key, Value);
        }

        public void SetState(string Key, float Value) {
            states.SetState(Key, Value);
        }

        public void SetState(string Key, Item Value) {
            states.SetState(Key, Value);
        }

        public void AddOne(string Key) {
            states.AddOne(Key);
        }

        protected virtual void Start() {
            player = GameObject.FindWithTag("Player");
            examineThoghut = player.GetComponentInChildren<Thoughts>();
            states = ScriptableObject.CreateInstance<AllStates>();
            move = player.GetComponent<ClickMovement>();
            foreach (var ls in boolList) {
                SetState(ls.Key, ls.Value);
            }
            foreach (var ls in floatList) {
                SetState(ls.Key, ls.Value);
            }
            /*foreach (var ls in itemList) {
                SetState(ls.Key, ls.Value);
            }*/ // use only with itemList
            lastExamine = examines[0];
        }

        public void ToExamine() {
            StartCoroutine(examineThoghut.thinkAloud(examines[Random.Range(0, examines.Length)]));
            SetState(StateKey.HasExamine.ToString(), true);
        }



        public Action currentAction() {
            foreach(var action in possibleActions) {
                if (action.ActionCanBeExecuted(this))
                    return action.action;
            }
            return null;
        }

        public bool isPlayerHere() {
            return ((Vector2)player.transform.position - playerLoactionNearTheObject()).magnitude < 0.01f;
        }

        public void walkHere() {
            Debug.Log("I am walking");
            Debug.Log("from " + player.transform.position + " to " + transform.position);
            move.changeLocation(playerLoactionNearTheObject());
           
        }

        protected virtual void Update() {
            if (isPlayerHere()) {
                examines[0] = lastExamine;
                SetState(StateKey.PlayerHere.ToString(), true);
            } else {
                SetState(StateKey.PlayerHere.ToString(), false);
            }
        }

        public Vector2 playerLoactionNearTheObject() {
            return new Vector2(transform.position.x, transform.position.y + BelowTheObject);
        }
    }
}