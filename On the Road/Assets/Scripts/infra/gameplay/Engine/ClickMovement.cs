﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace OnTheRoad {
    public class ClickMovement : MonoBehaviour {
        public float speed = 3f;

        // Public only for demonstration purposes.
        public Vector2 target;

        private Camera mainCamera;

        // Cache the transform component instead of using the provided property
        // 'transform' to avoid the implicit call to GetComponent every time.
        private new Transform transform;

        protected void Awake() {
            transform = GetComponent<Transform>();
            mainCamera = Camera.main;

            target = transform.position;

           // GameInputCapture.OnTouchUp += SetTargetFromTouch;
        }

        public void changeLocation(Transform t) {
            target = t.position;
            
        }

        public void changeLocation(Vector3 v) {
            target = v;

        }

        public void changeLocationByVector2(Vector2 v) {
            target = v;
        }

        protected void Update() {

            // Calculate distance to travel in this frame.
            var distance = speed * Time.deltaTime;

            // Update position.
            Vector2 position = transform.position;
            var direction = target - position;
            // Limit movement distance by calculated distance.
            if (direction.sqrMagnitude > distance * distance) {
                direction = direction.normalized * distance;
            }
            transform.position = position + direction;
        }
    }
}