﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace OnTheRoad {
    public abstract class OnTheRoadException : Exception {
        public OnTheRoadException() {
        }

        public OnTheRoadException(string message) : base(message) {
        }

        public OnTheRoadException(string message, Exception innerException) : base(message, innerException) {
        }

        protected OnTheRoadException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }


    [Serializable]
    internal class KeyNotFoundException : OnTheRoadException {
        public KeyNotFoundException() {
        }

        public KeyNotFoundException(string message) : base(message) {
        }

        public KeyNotFoundException(string message, Exception innerException) : base(message, innerException) {
        }

        protected KeyNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }

    [Serializable]
    internal class UnsupportedOperationException : OnTheRoadException {
        public UnsupportedOperationException() {
        }

        public UnsupportedOperationException(string message) : base(message) {
        }

        public UnsupportedOperationException(string message, Exception innerException) : base(message, innerException) {
        }

        protected UnsupportedOperationException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }

    [Serializable]
    internal class ItemNotFoundException : OnTheRoadException {
        public ItemNotFoundException() {
        }

        public ItemNotFoundException(string message) : base(message) {
        }

        public ItemNotFoundException(string message, Exception innerException) : base(message, innerException) {
        }

        protected ItemNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }
    }

}