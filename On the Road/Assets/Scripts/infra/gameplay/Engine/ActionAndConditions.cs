﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    [Serializable]
    public class ActionAndConditions {

        public Action action;

        public List<Condition> conditions;


        public bool ActionCanBeExecuted(InteractableObject IO) { 
            foreach(var condition in conditions) {
                if (condition != null && !condition.isMet(IO))
                    return false;
            }
            return true;
        }

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

    }
}
