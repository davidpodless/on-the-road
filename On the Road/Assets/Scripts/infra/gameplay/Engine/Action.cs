﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class Action : MonoBehaviour {

        [TextArea][SerializeField] string description;

        public string Description {
        get { return description; } }

        public List<Effect> effects;
        
        public void Excute(InteractableObject IO) {
            foreach(var effect in effects) {
                effect.Excute(IO);
            }
        }
        // Use this for initialization
        void Start() {
            description.Replace("\\n", "\n");

        }

        // Update is called once per frame
        void Update() {

        }
    }
}