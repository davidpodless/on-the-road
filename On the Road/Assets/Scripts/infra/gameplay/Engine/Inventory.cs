﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    class Inventory : MonoBehaviour {
        
        [SerializeField]
        List<Item> items;
        private Item itemInHand;
        public List<Item> Items { get { return items; } }
        public Item ItemInHand { get { return itemInHand; } }

        public void TakeItemToHand(string name) {
            try {
                if (itemInHand != null)
                    PutItemBackInInventory();
                itemInHand = GetItem(name);
                RemoveItem(itemInHand);
            } catch (Exception) {

            }
        }

        public void TakeItemToHand(int index) {
            try {
                if (itemInHand != null)
                    PutItemBackInInventory();
                itemInHand = GetItem(index);
                RemoveItem(itemInHand);
            } catch (Exception) { }
        }

        public void PutItemBackInInventory() {
            AddItem(itemInHand);
            itemInHand = null;
        }

        public void AddItem(Item item) {
            if(item != null && items.Count <= 6) items.Add(item);
        }

        public Item GetItem(string name) {
            foreach(var item in items) {
                if (item.NameOfItem.Equals(name))
                    return item;
            }
            throw new ItemNotFoundException();
        }

        public Item GetItem(int index) {
            try {
                return items[index];
            } catch (Exception) {
                throw new ItemNotFoundException();
            }

        }

        public bool RemoveItem(Item item) {
            return items.Remove(item);
        }

        public bool RemoveItem(string name) {
            foreach (var item in items) {
                if (item.NameOfItem.Equals(name))
                    return items.Remove(item);
            }
            throw new ItemNotFoundException();
        }

        public bool RemoveItem(int index) {
            try {
                items.RemoveAt(index);
                return true;
            } catch (Exception) { return false; }
        }

        private void Start() {
            itemInHand = null;
        }
    }
}
