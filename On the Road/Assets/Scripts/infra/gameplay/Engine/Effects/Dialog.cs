﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Infra.Utils;

namespace OnTheRoad {
    public class Dialog : Effect {
        [SerializeField] GameObject dialogUI;
        [SerializeField] Button[] buttons;
        [SerializeField] string dialogText;
        [SerializeField] string[] options;
        [EnumBasedString(typeof(StateKey))] [SerializeField] string state;
        [SerializeField] Effect[] nextDialog;
        private System.Action[] buttonsAction;
        private Text textGameObject;
        private InteractableObject playerIO;

        protected override void _Excute(InteractableObject IO) {
            dialogUI.SetActive(true);
            textGameObject.text = dialogText;
            for (int i = 0; i < options.Length; i++) {//TODO -  make it variable - how to add options
                int count = i;
                buttonsAction[i] = delegate () {
                    playerIO.SetState(state, count);
                    Debug.Log("the state " + state + " is " + playerIO.FindState(state));
                    FinishDialog();
                    if (nextDialog.Length > count && nextDialog[count] != null)
                        nextDialog[count].Excute(playerIO);
                    else
                        base._Excute(playerIO);
                };
                buttons[i].onClick.RemoveAllListeners();
                buttons[i].onClick.AddListener(() => buttonsAction[count]());
                
                buttons[i].gameObject.SetActive(true);
                buttons[i].GetComponentInChildren<Text>().text = options[i];
            }

        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
            buttonsAction = new System.Action[buttons.Length];
            textGameObject = dialogUI.GetComponentInChildren<Text>();
            playerIO = player.GetComponent<InteractableObject>();
            for(int i = 0; i< buttons.Length; i++) {
                buttons[i].gameObject.SetActive(false);
            }
        }

        private void FinishDialog() {
            foreach(var button in buttons) {
                button.gameObject.SetActive(false);
            }
            dialogUI.SetActive(false);
        }

        // Update is called once per frame
        void Update() {

        }
    }
}
