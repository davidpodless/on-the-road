﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infra.Utils;

namespace OnTheRoad {
    public class changePlayerStateBool : Effect {
        [EnumBasedString(typeof(StateKey))]
        [SerializeField] string key;
        [SerializeField] bool boolState;
        [SerializeField] float floatState;

        private enum boolOrFloat {
            Bool,
            Float,
        }
        [SerializeField]
        boolOrFloat choose;
        private InteractableObject playerIO;

        protected override void _Excute(InteractableObject IO) {
            switch (choose) {
                case boolOrFloat.Bool:
                    playerIO.SetState(key, boolState);
                    break;
                case boolOrFloat.Float:
                    playerIO.SetState(key, floatState);
                    break;
                default:
                    break;
            }
            
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
            playerIO = player.GetComponent<InteractableObject>();


        }

        // Update is called once per frame
        void Update() {

        }
    }
}
