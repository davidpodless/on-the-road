﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infra.Utils;

namespace OnTheRoad {
    public class AddOneToState : Effect {
        [EnumBasedString(typeof(StateKey))]
        [SerializeField] string key;


        protected override void _Excute(InteractableObject IO) {
            IO.AddOne(key);
            base._Excute(IO);
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();



        }

        // Update is called once per frame
        void Update() {

        }
    }
}
