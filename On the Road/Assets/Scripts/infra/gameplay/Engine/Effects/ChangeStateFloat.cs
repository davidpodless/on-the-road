﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Infra.Utils;

namespace OnTheRoad {
    public class ChangeStateFloat : Effect {
        [EnumBasedString(typeof(StateKey))]
        [SerializeField] string key;
        [SerializeField] float newState;

        protected override void _Excute(InteractableObject IO) {
            IO.SetState(key, newState);
            base._Excute(IO);
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();



        }

        // Update is called once per frame
        void Update() {

        }
    }
}
