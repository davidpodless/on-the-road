﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class Move : Effect {
        private ClickMovement move;


        protected override void _Excute(InteractableObject IO) {//strat moving
            IO.walkHere();
            StartCoroutine(tryAgain(IO));
        }

        private IEnumerator tryAgain(InteractableObject IO) {//check if destenation was changed
            yield return new WaitForSecondsRealtime(0.3f);
            continueMoving(IO);

        }
        private void continueMoving(InteractableObject IO) {//when the characther arrived - continue to the next effect.
            if (IO.playerLoactionNearTheObject() != move.target)
                return;
            else if (IO.isPlayerHere())
                base._Excute(IO);
            else
                StartCoroutine(tryAgain(IO));
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
            move = player.GetComponent<ClickMovement>();
        }

        // Update is called once per frame
        void Update() {

        }
    }
}
