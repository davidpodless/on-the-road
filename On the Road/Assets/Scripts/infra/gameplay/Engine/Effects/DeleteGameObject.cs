﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class DeleteGameObject : Effect {
        ClickMovement playerMove;
        Vector2 destination;

        protected override void _Excute(InteractableObject IO) {
            destination = playerMove.target;
            ExcutePhase2(IO);

        }

        private void ExcutePhase2(InteractableObject IO) {
            if (IO.isPlayerHere()) {
                IO.gameObject.SetActive(false);
                base._Excute(IO);
            } else {
                if (destination != playerMove.target)
                    return;
                StartCoroutine(waitForSec(0.3f, IO));

            }
        }

        public IEnumerator waitForSec(float seconds, InteractableObject IO) {
            yield return new WaitForSecondsRealtime(seconds);
            ExcutePhase2(IO);
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
            playerMove = player.GetComponent<ClickMovement>();

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
