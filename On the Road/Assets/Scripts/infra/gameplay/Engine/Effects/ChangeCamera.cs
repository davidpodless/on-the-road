﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class ChangeCamera : Effect {
        [SerializeField] Camera toActive;
        [SerializeField] Camera toShut;
        [SerializeField]
        InteractableObject playerLocation;
        

        protected override void _Excute(InteractableObject IO) {

                toActive.gameObject.SetActive(true);
                toShut.gameObject.SetActive(false);
                GameObject.FindWithTag("Canvas").GetComponentInChildren<Canvas>().worldCamera = toActive;
                player.transform.position = playerLocation.playerLoactionNearTheObject();
                var move = player.GetComponent<ClickMovement>();
                move.changeLocation(playerLocation.playerLoactionNearTheObject());
                base._Excute(IO);

        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
        }

        // Update is called once per frame
        void Update() {

        }
    }
}
