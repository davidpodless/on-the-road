﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Infra.Utils;

namespace OnTheRoad {
    public class Chat : Effect {
        [SerializeField] GameObject dialogUI;
        private Text textGameObject;
        [SerializeField]
        string[] dialogText;
        int i;


        protected override void _Excute(InteractableObject IO) {
            dialogUI.SetActive(true);
            if (i < dialogText.Length) {
                textGameObject.text = dialogText[i];
                i++;
                StartCoroutine(nextSentence(2.5f, IO));
            } else {
                FinishDialog();
                base._Excute(IO);
            }
                
        }

        private IEnumerator nextSentence(float seconds, InteractableObject IO) {
            yield return new WaitForSecondsRealtime(seconds);
            _Excute(IO);
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
            i = 0;
            textGameObject = dialogUI.GetComponentInChildren<Text>();
            //dialogUI.SetActive(false);
        }

        private void FinishDialog() {
            dialogUI.SetActive(false);
        }

        // Update is called once per frame
        void Update() {

        }
    }
}
