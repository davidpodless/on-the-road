﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class SetGameObjectActive : Effect {
        [SerializeField]
        List<GameObject> gameObjects;
        [SerializeField]
        bool setActive;

        protected override void _Excute(InteractableObject IO) {
            foreach(var gameobject in gameObjects)
                gameobject.SetActive(setActive);
            base._Excute(IO);
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
        }

        // Update is called once per frame
        void Update() {

        }
    }
}
