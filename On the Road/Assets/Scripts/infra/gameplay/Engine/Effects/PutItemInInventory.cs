﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class PutItemInInventory : Effect {
        private Inventory inventory;

        protected override void _Excute(InteractableObject IO) {
            inventory.PutItemBackInInventory();
            base._Excute(IO);

        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
            inventory = player.GetComponentInChildren<Inventory>();


        }

        // Update is called once per frame
        void Update() {

        }
    }
}
