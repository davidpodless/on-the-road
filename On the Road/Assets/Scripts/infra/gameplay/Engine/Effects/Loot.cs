﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class Loot : Effect {
        private Inventory inventory;
        [SerializeField]
        Item itemToLoot;
        protected override void _Excute(InteractableObject IO) {
            inventory.AddItem(itemToLoot);
            if (ThoughtAfterEffect == "")
                ThoughtAfterEffect = "I've found " + itemToLoot.NameOfItem;
            if (Examine == "")
                Examine = "there is nothing left there";
            base._Excute(IO);
            
        }

        // Use this for initialization
        protected override void Start() {
            base.Start();
            inventory = player.GetComponentInChildren<Inventory>();


        }

        // Update is called once per frame
        void Update() {

        }
    }
}
