﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    abstract public class Effect : MonoBehaviour {

        protected GameObject player;
        protected Thoughts examineThought;
        [SerializeField]
        string thoughtAfterEffect;
        [SerializeField]
        string changeExamineTo;
        [SerializeField]
        float delay;
        [SerializeField] protected
        Effect nextEffect;
        

        public void Excute(InteractableObject IO) {
            if (Delay != 0f) {
                StartCoroutine(delayExcute(Delay, IO));
            } else {
                _Excute(IO);
            }
            Delay = 0f;
        }

        protected virtual void _Excute(InteractableObject IO) {
            MakeThought();
            ChangeExamineTo(IO);
            if (nextEffect != null)
                nextEffect.Excute(IO);
        }

        private IEnumerator delayExcute(float seconds, InteractableObject IO) {
            yield return new WaitForSecondsRealtime(seconds);
            _Excute(IO);
        }


        protected string Examine { get { return changeExamineTo; } set { changeExamineTo = value; } }
        protected string ThoughtAfterEffect { get { return thoughtAfterEffect; } set { thoughtAfterEffect = value; } }
        protected float Delay { get { return delay; } set { delay = value; } }

        // Use this for initialization
        protected virtual void Start() {
            player = GameObject.FindWithTag("Player");
            examineThought = player.GetComponentInChildren<Thoughts>();

        }

        protected void MakeThought() {
            if(thoughtAfterEffect != "")
            StartCoroutine(examineThought.thinkAloud(thoughtAfterEffect));
        }

        protected void ChangeExamineTo(InteractableObject IO) {
            if(changeExamineTo != "")
            IO.Examine = changeExamineTo;
        }

        // Update is called once per frame
        void Update() {

        }
    }
}