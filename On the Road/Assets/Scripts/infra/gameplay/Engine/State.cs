﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace OnTheRoad {
    abstract class state<T> {
    //    [EnumBasedString(typeof(MyEnum))]
      //  [SerializeField]
        //string myEnumValue;

        protected Dictionary<string, T> stateDic = new Dictionary<string, T>();
        public void SetState(string key, T value) {
            stateDic[key] = value;
        }
        public T FindState(string key) {
            if (stateDic.ContainsKey(key))
                return stateDic[key];
            else
                throw new KeyNotFoundException();
        }

        public bool HasSomething() {
            return stateDic.Count > 0;
        }

        public bool RemoveKey(string Key) {
            return stateDic.Remove(Key);
        }
    }
}
