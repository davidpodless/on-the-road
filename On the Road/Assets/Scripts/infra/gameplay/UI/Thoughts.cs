﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    public class Thoughts : MonoBehaviour {

        protected TextMesh examineThoghut;

        // Use this for initialization
        void Start() {
            examineThoghut = GetComponentInChildren<TextMesh>();
            examineThoghut.text = "";
        }

        // Update is called once per frame
        void Update() {

        }

        public IEnumerator thinkAloud(string s) {
            examineThoghut.text = s;
            yield return new WaitForSecondsRealtime(2.5f);
            if (examineThoghut.text == s)
                examineThoghut.text = ""; //TODO - how to prevent it from delete something that wasn't suppose to be deleted yet?
        }

    }
}
