﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnTheRoad {
    [CreateAssetMenu()]
    public class mouseManagerHelper : ScriptableObject  {
        
        public enum button {
            moveTo,
            examine,
        } 

        private static button state;
        public static button getState() {
            return state;
        }
        public static void setState(button value) {
            state = value;
        }
        // Use this for initialization
        void Start() {
            setState(button.examine);

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
