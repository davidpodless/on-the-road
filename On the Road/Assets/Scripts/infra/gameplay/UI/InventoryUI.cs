﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OnTheRoad {
    public class InventoryUI : MonoBehaviour {
        private GameObject player;
        private Inventory inventory;
        private Image[] images;
        [SerializeField]
        Item emptySlot;

        private void Start() {
            player = GameObject.FindWithTag("Player");
            inventory = player.GetComponentInChildren<Inventory>();
            images = GetComponentsInChildren<Image>();
        }

        public void takeItem(int index) { 
            inventory.TakeItemToHand(index);
        }

        private void Update() {
            int i;
            for(i = 0; i < inventory.Items.Count; i++) {
                images[i].sprite = inventory.Items[i].InventoryIcon;
            }
            for(; i < images.Length; i++) {
                images[i].sprite = emptySlot.InventoryIcon;
            }
            
        }

    }
}
