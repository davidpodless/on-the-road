﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace OnTheRoad {
    public class OnMouseManager : MonoBehaviour {

        InteractableObject PoI;
        Text Description;

        void Start() {
            PoI = GetComponentInParent<InteractableObject>();
            Description = GameObject.FindWithTag("Description").GetComponentInChildren<Text>();
            Description.text = "";
        }

        public void OnMouseDown() {
         //   mouseManagerHelper.setState("ToExamine");
            Debug.Log("left click happened Down");
            

        }

        public void OnMouseUp() {
            Debug.Log("left click happened Up");
            Debug.Log("This object was examined on " + Time.time);
            PoI.ToExamine();
        }

        public void OnMouseExit() {
            Description.text = "";
        }

        public void OnMouseOver() {
            if (PoI.currentAction() != null) {
                Description.text = PoI.currentAction().Description;
                if (Input.GetMouseButtonUp(1)) {

                    Debug.Log("Right click happed Up");
                    PoI.currentAction().Excute(PoI);

                }
            } else
                Description.text = "Nothing to do here\nright now";
        }
    }
}